const getSum = (str1, str2) => {
    if (isNaN(parseInt(str1))) {
        str1 = 0;
    }
    if (isNaN(parseInt(str2))) {
        str2 = 0;
    }
    if (parseInt(str1).toString().length !== str1.toString().length || parseInt(str2).toString().length !== str2.toString().length) {
        return false;
    }
    let res = parseInt(str1) + parseInt(str2);
    return res === 0 ? false : res.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let returnable = {Post: 0, comments: 0};
    for (let post of listOfPosts) {
        if (post.author === authorName) {
            returnable.Post++;
        }
        if (!post.hasOwnProperty('comments')) {
            continue;
        }
        for (let comment of post.comments) {
            if (comment.author === authorName) {
                returnable.comments++;
            }
        }
    }
    return "Post:" + returnable.Post + ",comments:" + returnable.comments;
};

const tickets = (people) => {
    let myMoney = 0;
    let price = 25;
    for (let payment of people) {
        if (parseInt(payment) - price - myMoney > 0) {
            return "NO";
        }
        myMoney += 2 * price - parseInt(payment);
    }
    return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
